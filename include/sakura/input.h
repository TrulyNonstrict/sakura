#ifndef SAKURA_INPUT_H
#define SAKURA_INPUT_H

#include "sdl.h"
#include <functional>
#include <vector>

namespace sakura {
    class Input
    {
    public:
        Input();
        bool quit() const;
        void update();

        const std::vector<SDL_Event>& events() const;
    private:
        bool m_quit;
        std::vector<SDL_Event> m_events;
    };
}

#endif
