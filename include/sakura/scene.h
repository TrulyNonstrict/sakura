#ifndef SAKURA_SCENE_H
#define SAKURA_SCENE_H

#include <forward_list>

namespace sakura {
    class Entity;
    class Scene
    {
    public:
        virtual ~Scene() = default;
        const std::forward_list<Entity*>& entity_queue();
        void clear_entity_queue();
    protected:
        void add_entity(Entity&);
    private:
        std::forward_list<Entity*> m_entities;
    };
}

#endif
