#ifndef SAKURA_FONT_H
#define SAKURA_FONT_H

#include "sdl.h"
#include <array>
#include <string>
#include <vector>

namespace sakura {
    class Renderer;
    class Font
    {
    public:
        Font(Renderer&, TTF_Font&, SDL_Color);

        void render(Renderer&, const std::string&, int, int) const;
    private:
        struct Glyph
        {
            std::vector<sdl::Unique_Texture>::size_type texture;
            SDL_Rect rect;
        };

        static constexpr Uint16 printable_chars_begin = 20;
        std::vector<sdl::Unique_Texture> m_textures;
        std::array<Glyph, 128 - printable_chars_begin> m_cache;
    };
}

#endif
