#ifndef SAKURA_SDL_H
#define SAKURA_SDL_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#ifdef __EMSCRIPTEN__
#include <SDL_ttf.h>
#else
#include <SDL2/SDL_ttf.h>
#endif
#include <memory>

namespace sakura {
    namespace sdl {

        struct Mask
        {
            Uint32 r;
            Uint32 g;
            Uint32 b;
            Uint32 a;
        };

        Mask mask();

        /**
           RAII for X_Init and X_Quit functions for each X in {SDL, TTF, IMG}
         */

        struct Initialize
        {
            Initialize();
            virtual ~Initialize();
        };

        /**
           Wrapper functors that can act as deleters for std::unique_ptrs that
           manage pointers acquired from SDL:
         */

        struct Destroy_Window
        {
            void operator()(SDL_Window*) const;
        };

        struct Destroy_Texture
        {
            void operator()(SDL_Texture*) const;
        };

        struct Free_Surface
        {
            void operator()(SDL_Surface*) const;
        };

        struct Destroy_Renderer
        {
            void operator()(SDL_Renderer*) const;
        };

        struct Close_Font
        {
            void operator()(TTF_Font*) const;
        };

        /**
           Utility type aliases for managed SDL resources
         */

        using Unique_Window = std::unique_ptr<SDL_Window, Destroy_Window>;
        using Unique_Texture = std::unique_ptr<SDL_Texture, Destroy_Texture>;
        using Unique_Surface = std::unique_ptr<SDL_Surface, Free_Surface>;
        using Unique_Renderer = std::unique_ptr<SDL_Renderer, Destroy_Renderer>;
        using Unique_Font = std::unique_ptr<TTF_Font, Close_Font>;

        Unique_Surface load_image(const std::string&);
    }
}

#endif
