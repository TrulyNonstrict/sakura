#ifndef EXAMPLE1_ENTITY_H
#define EXAMPLE1_ENTITY_H

#include <sakura/entity.h>
#include <sakura/sdl.h>

namespace example1 {
    class Entity : public sakura::Entity
    {
    public:
        void update(sakura::App&) override;
    private:
        sakura::sdl::Unique_Texture m_texture;
    };
}

#endif
