#include "stylesheet.h"
#include <sakura/sdl.h>

sakura::style::Sheet example1::stylesheet()
{
    sakura::style::Sheet stylesheet;
    SDL_Color blue = {0, 0, 255, 255};
    stylesheet.fonts = {
        {"main", {"_assets/font.ttf", 16, blue}}
    };
    return stylesheet;
}
