#ifndef EXAMPLE1_SCENE_H
#define EXAMPLE1_SCENE_H

#include "entity.h"
#include <sakura/scene.h>

namespace example1 {
    class Scene : public sakura::Scene
    {
    public:
        Scene();
    private:
        Entity m_entity;
    };
}

#endif
