#include "entity.h"
#include <sakura/app.h>
#include <iostream>

void example1::Entity::update(sakura::App& app)
{
    if(m_texture.get() == nullptr) {
        m_texture = app.renderer().create_texture_from_surface(
            *sakura::sdl::load_image("_assets/image.png")
        );
    }

    app.renderer().draw_texture(*m_texture, 10, 10);
    app.theme().font_at("main").render(app.renderer(), "Hello world!", 30, 30);
    app.renderer().draw_line(0, 0, 30, 30);
}
