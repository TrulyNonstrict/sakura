#include "sakura/app.h"
#include "sakura/entity.h"
#include "sakura/sdl.h"
#include "sakura/style/theme.h"

#include <vector>
#ifdef __EMSCRIPTEN__
#include <emscripten.h>
#endif

void sakura::App::change_scene(std::unique_ptr<sakura::Scene> scene)
{
    m_next_scene = std::move(scene);
}

void sakura::App::emscr_main_loop(void* arg)
{
    static_cast<sakura::App*>(arg)->tick();
}

sakura::App::App(
    std::unique_ptr<sakura::Scene> scene,
    sakura::style::Sheet sheet,
    sakura::Input input,
    sakura::Renderer renderer)
    : m_scene(std::move(scene)),
      m_theme(m_renderer, sheet),
      m_input(std::move(input)),
      m_renderer(std::move(renderer))
{
}

sakura::style::Theme& sakura::App::theme()
{
    return m_theme;
}

sakura::Input& sakura::App::input()
{
    return m_input;
}

sakura::Renderer& sakura::App::renderer()
{
    return m_renderer;
}

void sakura::App::tick()
{
    m_renderer.preupdate();

    for(auto& entity : m_scene->entity_queue()) {
        entity->add_observer(m_entities);
    }
    m_scene->clear_entity_queue();

    m_input.update();

    for(auto& entity : m_entities.subjects()) {
        for(auto& event : m_input.events()) {
            entity->input(event);
        }
        entity->update(*this);
    }

    m_renderer.update();

    if(m_next_scene.get() != nullptr) {
        m_scene = std::move(m_next_scene);
        m_next_scene = std::unique_ptr<sakura::Scene>();
    }
}

// On the web, the destructors of stack-allocated objects run before the main
// loop starts, so give the objects used by the game loop's state static
// storage duration
#ifdef __EMSCRIPTEN__
#define MAYBE_STATIC static
#else
#define MAYBE_STATIC
#endif

int sakura::App::main(
    const sakura::style::Sheet& stylesheet,
    std::unique_ptr<sakura::Scene> scene_param,
    const std::string& name,
    int width,
    int height,
    Uint32 flags)
{
    MAYBE_STATIC sakura::sdl::Initialize init;
    MAYBE_STATIC sakura::App state(
        std::move(scene_param),
        stylesheet,
        sakura::Input(),
        sakura::Renderer(name, width, height, flags)
    );

#ifdef __EMSCRIPTEN__
    emscripten_set_main_loop_arg(emscr_main_loop, &state, -1, true);
#else
    while(!state.input().quit()) {
        state.tick();
    }
#endif

    return 0;
}
