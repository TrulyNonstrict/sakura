#include "sakura/style/sheet.h"

sakura::style::Sheet::Sheet(
    std::unordered_map<std::string, sakura::style::Font> p_fonts
)
    : fonts(std::move(p_fonts))
{
}
