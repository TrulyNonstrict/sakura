#include "sakura/input.h"

sakura::Input::Input()
    : m_quit(false)
{
}

bool sakura::Input::quit() const
{
    return m_quit;
}

void sakura::Input::update()
{
    m_quit = false;
    SDL_Event event;
    m_events.clear();

    while(SDL_PollEvent(&event)) {
        switch(event.type) {
        case SDL_QUIT:
            m_quit = true;
        default:
            m_events.emplace_back(event);
        }
    }
}

const std::vector<SDL_Event>& sakura::Input::events() const
{
    return m_events;
}
