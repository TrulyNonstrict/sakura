#include <sakura/font.h>
#include <sakura/renderer.h>
#include <stdexcept>

sakura::Font::Font(sakura::Renderer& renderer, TTF_Font& font, SDL_Color color)
{
    int x = 0;
    int y = 0;
    const int dim = 128; // length and width of texture
    int max_height = 0;

    auto mask = sakura::sdl::mask();
    auto dest_surface = sakura::sdl::Unique_Surface(
        SDL_CreateRGBSurface(0, dim, dim, 32, mask.r, mask.g, mask.b, mask.a)
    );

    for(Uint16 c = printable_chars_begin; c < 128; ++c) {
        auto glyph_surface = sakura::sdl::Unique_Surface(
            TTF_RenderGlyph_Solid(&font, c, color)
        );

        if(glyph_surface->w > dim || glyph_surface->h > dim) {
            throw std::length_error(
                "Font character exceeds texture dimensions. Dimensions: "
                + std::to_string(glyph_surface->w) + "x"
                + std::to_string(glyph_surface->h)
            );
        }

        if(x + glyph_surface->w > dim) {
            // Go to the next line
            x = 0;
            y += max_height;
            max_height = 0;
            if(y + glyph_surface->h > dim) {
                // Push current dest surface onto vector and create new surface
                auto texture =
                    renderer.create_texture_from_surface(*dest_surface);
                m_textures.emplace_back(std::move(texture));
                dest_surface = sakura::sdl::Unique_Surface(
                    SDL_CreateRGBSurface(
                        0, dim, dim, 32, mask.r, mask.g, mask.b, mask.a
                    )
                );
                y = 0;
            }
        }

        if(glyph_surface->h > max_height) {
            max_height = glyph_surface->h;
        }

        SDL_Rect dest_rect;
        dest_rect.x = x;
        dest_rect.y = y;
        dest_rect.w = glyph_surface->w;
        dest_rect.h = glyph_surface->h;
        SDL_BlitSurface(
            glyph_surface.get(), nullptr, dest_surface.get(), &dest_rect
        );
        x += glyph_surface->w;
        Glyph glyph = { m_textures.size(), dest_rect };
        m_cache[c - printable_chars_begin] = glyph;
    }

    auto texture = renderer.create_texture_from_surface(*dest_surface);
    m_textures.emplace_back(std::move(texture));
}

void sakura::Font::render(
    sakura::Renderer& renderer,
    const std::string& str,
    int x,
    int y) const
{
    for(char c : str) {
        if(c < printable_chars_begin) {
            continue;
        }
        const Glyph& glyph = m_cache.at(c - printable_chars_begin);

        renderer.draw_subtexture(
            *m_textures[glyph.texture], x, y, glyph.rect
        );
        x += glyph.rect.w;
    }
}
