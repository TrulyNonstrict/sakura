#include "sakura/entity.h"
#include "sakura/app.h"

void sakura::Entity::add_observer(sakura::Observer<sakura::Entity>& obs)
{
    m_subject.add_observer(*this, obs);
}

void sakura::Entity::input(const SDL_Event&)
{
}

void sakura::Entity::update(sakura::App&)
{
}
